/*
 * @Author: pardon110
 * @Date: 2024-03-25 17:39:09
 * @LastEditors: pardon110@qq.com
 * @LastEditTime: 2024-03-28 18:03:31
 * @FilePath: \days7\tests\reflect_test.go
 * @Description: go test -v -count=1 days7.test/tests
 */

package tests

import (
	"fmt"
	"reflect"
	"testing"
)

type Account struct {
	Username string
	Password string
}

func Test_reflect_struct(t *testing.T) {
	typ := reflect.Indirect(reflect.ValueOf(&Account{})).Type()
	fmt.Println(typ.Name()) // Account

	for i := 0; i < typ.NumField(); i++ {
		field := typ.Field(i)
		fmt.Println(field.Name) // Username Password
	}

}

type Person struct {
	Name string
	Age  int
}

// go test -v -count=1 -run ^Test_reflect_struct_name$ days7.test/tests
func Test_reflect_struct_FieldByName(t *testing.T) {
	p := Person{Name: "Alice", Age: 30}
	v := reflect.ValueOf(p)

	fieldName := "Age"
	fieldValue := v.FieldByName(fieldName)

	if fieldValue.IsValid() {
		fmt.Printf("Field '%s' value: %v\n", fieldName, fieldValue.Interface())
	} else {
		fmt.Printf("Field '%s' not found\n", fieldName)
	}
}

// go test -v -count=1 -run ^Test_reflect_Type_Pkg_Name$ days7.test/tests
func Test_reflect_Type_Name(t *testing.T) {
	var p Person
	modelType := reflect.TypeOf(p)
	fmt.Println("Testing type Name: ", modelType.Name())
}

// go test -v -count=1 -run ^Test_defer$ days7.test/tests
func Test_defer(t *testing.T) {
	for i := 0; i < 3; i++ {
		defer fmt.Println(i) // 注册deferred 函数
	}

	defer t.Log("Testing defer func")

	for i := 0; i < 3; i++ {
		fmt.Println(i)
	}
}
