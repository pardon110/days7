package tests

import (
	"database/sql"
	"log"
	"testing"

	_ "github.com/mattn/go-sqlite3"
)

// go test -v -count=1 days7.test/tests
// go test -v -count=1 -run ^Test_Sql$ days7.test/tests
func Test_Sql(t *testing.T) {
	db, _ := sql.Open("sqlite3", "test.db")
	defer db.Close()
	db.Exec("DROP TABLE IF EXISTS User;")
	db.Exec("CREATE TABLE User(Name text);")

	result, err := db.Exec("INSERT INTO User(`Name`) values (?), (?)", "Tom", "Sam")
	if err == nil {
		affected, _ := result.RowsAffected()
		log.Println(affected)
	}

	row := db.QueryRow("select * from User limit 1")
	var name string
	if err := row.Scan(&name); err == nil {
		log.Println(name)
	}
}
