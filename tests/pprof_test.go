/*
 * @Author: pardon110
 * @Date: 2024-03-28 15:30:07
 * @LastEditors: pardon110@qq.com
 * @LastEditTime: 2024-03-28 15:41:37
 * @FilePath: \days7\tests\pprof_test.go
 * @Description:
 */
package tests

import "testing"

func fib(n int) int {
	if n == 0 || n == 1 {
		return n
	}
	return fib(n-2) + fib(n-1)
}

// go test -bench="Fib$" -cpuprofile cpu.pprof days7.test/tests
// go test  -benchmem -run=^$ -bench ^BenchmarkFib$ days7.test/tests
func BenchmarkFib(b *testing.B) {
	for n := 0; n < b.N; n++ {
		fib(30) // run fib(30) b.N times
	}
}
