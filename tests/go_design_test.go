/*
 * @Author: pardon110
 * @Date: 2024-03-28 18:01:21
 * @LastEditors: pardon110@qq.com
 * @LastEditTime: 2024-03-28 18:33:06
 * @FilePath: \days7\tests\go_design_test.go
 * @Description:
 */
package tests

import (
	"reflect"
	"sync"
	"testing"
)

type data struct {
	sync.Mutex
	buf [1024]byte
}

func Test_embed_method(t *testing.T) {
	d := data{}
	d.Lock() // sync.(*Mutex).Lock()
	defer d.Unlock()
	_ = d.buf
}

type T int

func (t T) A()  {}
func (t T) B()  {}
func (t *T) C() {}
func (t *T) D() {}

// go test -v  -count=1 -run ^Test_T_set$ days7.test/tests
func Test_T_set(t *testing.T) {

	show := func(i interface{}) {
		typ := reflect.TypeOf(i)
		for i := 0; i < typ.NumMethod(); i++ {
			t.Log(typ.Method(i).Name)
		}
	}

	var n T = 1
	var p *T = &n

	t.Log(" T method set..")
	show(n)
	t.Log(" *T method set..")
	show(p)
}

func Test_alias_method(t *testing.T) {

}
