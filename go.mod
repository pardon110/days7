module days7.test

go 1.20

require (
	github.com/mattn/go-sqlite3 v1.14.22
	github.com/pkg/profile v1.7.0
)

require (
	github.com/felixge/fgprof v0.9.3 // indirect
	github.com/google/pprof v0.0.0-20211214055906-6f57359322fd // indirect
)
