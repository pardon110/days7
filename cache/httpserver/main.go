/*
 * @Author: pardon110
 * @Date: 2024-03-19 17:25:23
 * @LastEditors: pardon110@qq.com
 * @LastEditTime: 2024-03-19 18:06:30
 * @FilePath: \days7\cache\httpserver\main.go
 * @Description:
 */
package main

import (
	"fmt"
	"log"
	"net/http"

	"days7.test/cache/httpserver/httpcache"
	"days7.test/cache/singlenode"
)

var db = map[string]string{
	"Tom":  "630",
	"Jack": "589",
	"Sam":  "567",
}

func main() {
	singlenode.NewGroup("scores", 2<<10, singlenode.GetterFunc(
		func(key string) ([]byte, error) {
			log.Println("SlowDB] searching key: ", key)
			if v, ok := db[key]; ok {
				return []byte(v), nil
			}
			return nil, fmt.Errorf("%s not exist", key)
		},
	))

	addr := "localhost:8080"
	peers := httpcache.NewHTTPPool(addr)

	log.Println("httpcache is running at", addr)
	log.Fatal(http.ListenAndServe(addr, peers))
}
