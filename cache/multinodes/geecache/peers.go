/*
 * @Author: pardon110
 * @Date: 2024-03-20 11:38:12
 * @LastEditors: pardon110@qq.com
 * @LastEditTime: 2024-03-20 11:41:38
 * @FilePath: \days7\cache\multinodes\geecache\peers.go
 * @Description:
 */
package geecache

// the peer that owns a specific key. must be implemented to locate
type PeerPicker interface {
	PickPeer(key string) (peer PeerGetter, ok bool)
}

type PeerGetter interface {
	Get(group string, key string) ([]byte, error)
}
