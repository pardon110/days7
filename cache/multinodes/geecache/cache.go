/*
 * @Author: pardon110
 * @Date: 2024-03-20 11:37:42
 * @LastEditors: pardon110@qq.com
 * @LastEditTime: 2024-03-20 11:44:49
 * @FilePath: \days7\cache\multinodes\geecache\cache.go
 * @Description:
 */
package geecache

import (
	"sync"

	"days7.test/cache/lru"
)

type cache struct {
	mu         sync.Mutex
	lru        *lru.Cache
	cacheBytes int64
}

func (c *cache) add(key string, value ByteView) {
	c.mu.Lock()
	defer c.mu.Unlock()
	if c.lru == nil {
		c.lru = lru.New(c.cacheBytes, nil)
	}
	c.lru.Add(key, value)
}

func (c *cache) get(key string) (value ByteView, ok bool) {
	c.mu.Lock()
	defer c.mu.Unlock()
	if c.lru == nil {
		return
	}

	if v, ok := c.lru.Get(key); ok {
		return v.(ByteView), ok
	}

	return
}
