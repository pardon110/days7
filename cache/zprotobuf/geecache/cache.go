/*
 * @Author: pardon110
 * @Date: 2024-03-21 17:19:39
 * @LastEditors: pardon110@qq.com
 * @LastEditTime: 2024-03-21 19:16:02
 * @FilePath: \days7\cache\zprotobuf\geecache\cache.go
 * @Description: 并发安全
 */
package geecache

import (
	"geecache/lru"
	"sync"
)

type cache struct {
	mu         sync.Mutex
	lru        *lru.Cache
	cacheBytes int64
}

func (c *cache) add(key string, value ByteView) {
	c.mu.Lock()
	defer c.mu.Unlock()
	if c.lru == nil { // 隋性加载
		c.lru = lru.New(c.cacheBytes, nil)
	}
	c.lru.Add(key, value)
}

func (c *cache) get(key string) (value ByteView, ok bool) {
	c.mu.Lock()
	defer c.mu.Unlock()
	if c.lru == nil {
		return
	}

	if v, ok := c.lru.Get(key); ok {
		return v.(ByteView), ok
	}

	return
}
