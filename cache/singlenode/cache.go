/*
 * @Author: pardon110
 * @Date: 2024-03-19 11:34:11
 * @LastEditors: pardon110@qq.com
 * @LastEditTime: 2024-03-19 15:31:45
 * @FilePath: \days7\cache\singlenode\cache.go
 * @Description: 并发控制,使用独占锁
 */
package singlenode

import (
	"sync"

	"days7.test/cache/lru"
)

// private intinonal sructure
type cache struct {
	mu         sync.Mutex
	lru        *lru.Cache
	cacheBytes int64
}

// add 并发控制
func (c *cache) add(key string, value ByteView) {
	c.mu.Lock()
	defer c.mu.Unlock()
	if c.lru == nil {
		c.lru = lru.New(c.cacheBytes, nil)
	}
	c.lru.Add(key, value)
}

func (c *cache) get(key string) (value ByteView, ok bool) {
	c.mu.Lock()
	defer c.mu.Unlock()
	if c.lru == nil {
		return
	}

	if v, ok := c.lru.Get(key); ok {
		return v.(ByteView), ok
	}

	return
}
