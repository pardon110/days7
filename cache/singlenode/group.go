/*
 * @Author: pardon110
 * @Date: 2024-03-19 11:36:00
 * @LastEditors: pardon110@qq.com
 * @LastEditTime: 2024-03-19 16:57:45
 * @FilePath: \days7\cache\singlenode\group.go
 * @Description:
 */
package singlenode

import (
	"fmt"
	"log"
	"sync"
)

// Group is a cache namespace and associated data loaded spread over multiple
type Group struct {
	name      string // 缓存命名空间
	getter    Getter // 缓存未命中时，获取数据源的回调
	mainCache cache  // 缓存实例,即一开始实现的并发缓存
}

// A Getter loads data for a key.
type Getter interface {
	Get(key string) ([]byte, error)
}

// key string -> bytes
// A GetterFunc implements Getter with a function
type GetterFunc func(key string) ([]byte, error)

// Get implements Getter interface function
func (f GetterFunc) Get(key string) ([]byte, error) {
	return f(key)
}

var (
	mu     sync.RWMutex              // 读保护
	groups = make(map[string]*Group) // 分组实例哈希
)

// NewGroup create a new instance of Group
func NewGroup(name string, cacheBytes int64, getter Getter) *Group {
	if getter == nil {
		panic("nil getter")
	}
	mu.Lock() // 创建独占锁
	defer mu.Unlock()

	g := &Group{
		name:      name,
		getter:    getter,
		mainCache: cache{cacheBytes: cacheBytes},
	}

	groups[name] = g

	return g
}

// 读取分组实例
func GetGroup(name string) *Group {
	mu.RLock()
	g := groups[name]
	mu.RUnlock()
	return g
}

// Get value for a key from  cache
func (g *Group) Get(key string) (ByteView, error) {
	if key == "" {
		return ByteView{}, fmt.Errorf("key is required")
	}

	if v, ok := g.mainCache.get(key); ok {
		log.Println("[SingleCache] hit")
		return v, nil
	}

	return g.load(key)
}

func (g *Group) load(key string) (value ByteView, err error) {
	return g.getLocally(key)
}

func (g *Group) getLocally(key string) (ByteView, error) {
	bytes, err := g.getter.Get(key)
	if err != nil {
		return ByteView{}, err
	}

	value := ByteView{b: cloneBytes(bytes)}
	g.populateCache(key, value)

	return value, nil
}

func (g *Group) populateCache(key string, value ByteView) {
	g.mainCache.add(key, value)
}
