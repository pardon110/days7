/*
 * @Author: pardon110
 * @Date: 2024-03-19 11:35:03
 * @LastEditors: pardon110@qq.com
 * @LastEditTime: 2024-03-19 15:13:08
 * @FilePath: \days7\cache\singlenode\byteview.go
 * @Description:  只读数据结构，只拥有值方法
 */
package singlenode

// A ByteView holds an immutable view of bytes.
type ByteView struct {
	b []byte
}

// implements lru.Value interface
func (v ByteView) Len() int { return len(v.b) }

// ByteSlices to expose
func (v ByteView) ByteSlice() []byte {
	return cloneBytes(v.b)
}

// cloneBytes private method
func cloneBytes(b []byte) []byte {
	c := make([]byte, len(b))
	copy(c, b)
	return c
}

// String returns the data as a string, making a copy if necessary
func (v ByteView) String() string {
	return string(v.b)
}
