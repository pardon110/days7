/*
 * @Author: pardon110
 * @Date: 2024-03-21 15:08:04
 * @LastEditors: pardon110@qq.com
 * @LastEditTime: 2024-03-21 15:56:21
 * @FilePath: \days7\cache\singleflight\singleflight_test.go
 * @Description:
 */

package singleflight

import (
	"testing"
)

func TestGroup_Do(t *testing.T) {
	var g Group
	v, err := g.Do("key", func() (interface{}, error) {
		return "bar", nil
	})

	if v != "bar" || err != nil {
		t.Errorf("Do v = %v, error = %v", v, err)
	}
}
