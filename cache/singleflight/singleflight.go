/*
 * @Author: pardon110
 * @Date: 2024-03-21 15:08:04
 * @LastEditors: pardon110@qq.com
 * @LastEditTime: 2024-03-21 16:26:53
 * @FilePath: \days7\cache\singleflight\singleflight.go
 * @Description:
 */
package singleflight

import "sync"

type call struct {
	wg  sync.WaitGroup
	val interface{}
	err error
}

// Group represents a class of work and forms a namespace in which
// uints of work can be executed with duplicate suppression.
type Group struct {
	mu sync.Mutex       // protects
	m  map[string]*call // lazily initialized
}

// Do executes and returns the results of the given function, making
// sure that only one execution is in-flight for a given key at a
// time.
func (g *Group) Do(key string, fn func() (interface{}, error)) (interface{}, error) {
	// 尽量减小临界区大小
	g.mu.Lock()
	if g.m == nil {
		g.m = make(map[string]*call) // 延迟初始化，提高内存效率
	}
	if c, ok := g.m[key]; ok {
		g.mu.Unlock()
		c.wg.Wait() // 若是请求正在进行中,则等待
		return c.val, c.err
	}

	c := new(call)
	c.wg.Add(1) // 发起请求前加锁
	g.m[key] = c
	g.mu.Unlock()

	c.val, c.err = fn() // 调用fn，发起请求

	g.mu.Lock()
	delete(g.m, key) // 更新 g.m
	g.mu.Unlock()

	c.wg.Done()
	return c.val, c.err
}
