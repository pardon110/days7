/*
 * @Author: pardon110
 * @Date: 2024-03-16 19:09:52
 * @LastEditors: pardon110@qq.com
 * @LastEditTime: 2024-03-17 16:35:09
 * @FilePath: \days7\web\router\trie\trie.go
 * @Description:
 */
package trie

import (
	"fmt"
	"strings"
)

type node struct {
	pattern  string  // 不完全路径字符串
	part     string  // 节点部分信息
	children []*node // 子节点
	isWild   bool    // 是否模糊匹配
}

func (n *node) String() string {
	return fmt.Sprintf("node[pattern=%s, part=%s, iswild=%t]", n.pattern, n.part, n.isWild)
}

func (n *node) insert(pattern string, parts []string, height int) {
	if len(parts) == height {
		n.pattern = pattern
		return
	}

	part := parts[height]
	child := n.matchChild(part)
	if child == nil {
		child = &node{part: part, isWild: part[0] == ':' || part[0] == '*'}
		n.children = append(n.children, child)
	}

	child.insert(pattern, parts, height+1)
}

func (n *node) search(parts []string, height int) *node {
	if len(parts) == height || strings.HasPrefix(n.part, "*") {
		if n.pattern == "" {
			return nil
		}
		return n
	}

	part := parts[height]
	children := n.matchChildren(part)

	for _, child := range children {
		result := child.search(parts, height+1)
		if result != nil {
			return result
		}
	}

	return nil
}

func (n *node) travel(list *([]*node)) {
	if n.pattern != "" {
		*list = append(*list, n)
	}

	for _, child := range n.children {
		child.travel(list)
	}
}

func (n *node) matchChild(part string) *node {
	for _, child := range n.children {
		if child.part == part || child.isWild {
			return child
		}
	}
	return nil
}

func (n *node) matchChildren(part string) []*node {
	nodes := make([]*node, 0)
	for _, child := range n.children {
		if child.part == part || child.isWild {
			nodes = append(nodes, child)
		}
	}
	return nodes
}
