/*
 * @Author: pardon110
 * @Date: 2024-03-16 19:05:33
 * @LastEditors: pardon110@qq.com
 * @LastEditTime: 2024-03-17 16:43:27
 * @FilePath: \days7\web\router\main.go
 * @Description:
 */
package main

import (
	"net/http"

	"days7.test/web/router/trie"
)

func main() {
	r := trie.New()
	r.GET("/", func(c *trie.Context) {
		c.HTML(http.StatusOK, "Hello trie!")
	})

	r.GET("/hello", func(c *trie.Context) {
		// expect /hello?name=pardon110
		c.String(http.StatusOK, "hello %s, you're at %s\n", c.Query("name"), c.Path)
	})

	r.GET("/hello/:name", func(c *trie.Context) {
		// expect /hello/pardon110
		c.String(http.StatusOK, "hello %s, you're at %s\n", c.Param("name"), c.Path)
	})

	r.GET("/assets/*filepath", func(c *trie.Context) {
		c.JSON(http.StatusOK, trie.H{"filepath": c.Param("filepath")})
	})

	r.Run(":8080")
}
