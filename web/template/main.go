/*
 * @Author: pardon110
 * @Date: 2024-03-18 11:30:32
 * @LastEditors: pardon110@qq.com
 * @LastEditTime: 2024-03-18 15:36:09
 * @FilePath: \days7\web\template\main.go
 * @Description:
 */
package main

import (
	"fmt"
	"html/template"
	"net/http"
	"time"

	"days7.test/web/template/ssr"
)

type student struct {
	Name string
	Age  int8
}

func FormatAsDate(t time.Time) string {
	year, month, day := t.Date()
	return fmt.Sprintf("%d-%02d-%02d", year, month, day)
}

func main() {
	r := ssr.New()
	r.Use(ssr.Logger())
	r.SetFuncMap(template.FuncMap{
		"FormatAsDate": FormatAsDate,
	})

	r.LoadHTMLGlob("templates/*")
	r.Static("/assets", "./static")

	stu1 := &student{Name: "Pardon110", Age: 20}
	stu2 := &student{Name: "Jack", Age: 22}

	r.GET("/", func(c *ssr.Context) {
		c.HTML(http.StatusOK, "css.tmpl", nil)
	})
	r.GET("/students", func(c *ssr.Context) {
		c.HTML(http.StatusOK, "arr.tmpl", ssr.H{
			"title":  "pardon110",
			"stuArr": [2]*student{stu1, stu2},
		})
	})

	r.GET("/date", func(c *ssr.Context) {
		c.HTML(http.StatusOK, "custom_func.tmpl", ssr.H{
			"title": "pardon110",
			"now":   time.Date(2019, 8, 17, 0, 0, 0, 0, time.UTC),
		})
	})

	r.Run(":8080")
}
