/*
 * @Author: pardon110
 * @Date: 2024-03-18 16:09:24
 * @LastEditors: pardon110@qq.com
 * @LastEditTime: 2024-03-18 16:17:55
 * @FilePath: \days7\web\recover\main.go
 * @Description:
 */
package main

import (
	"net/http"

	"days7.test/web/template/ssr"
)

func main() {
	r := ssr.Default()
	r.GET("/", func(c *ssr.Context) {
		c.String(http.StatusOK, "hello recover!")
	})

	r.GET("/panic", func(c *ssr.Context) {
		names := []string{"pardon9527"}
		c.String(http.StatusOK, names[70])
	})

	r.Run(":8080")
}
