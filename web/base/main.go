/*
 * @Author: pardon110
 * @Date: 2024-03-16 11:17:52
 * @LastEditors: pardon110@qq.com
 * @LastEditTime: 2024-03-16 11:52:03
 * @FilePath: \days7\web\base\main.go
 * @Description:
 */
package main

import (
	"fmt"
	"net/http"

	"days7.test/web/base/gee"
)

func main() {
	r := gee.New()

	r.GET("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "URL.Path = %q\n", r.URL.Path)
	})

	r.GET("/hello", func(w http.ResponseWriter, r *http.Request) {
		for k, v := range r.Header {
			fmt.Fprintf(w, "Header[%q]: %q\n", k, v)
		}
	})

	r.Run(":8080")
}
