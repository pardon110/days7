/*
 * @Author: pardon110
 * @Date: 2024-03-16 12:29:45
 * @LastEditors: pardon110@qq.com
 * @LastEditTime: 2024-03-16 18:04:31
 * @FilePath: \days7\web\context\main.go
 * @Description:
 */
package main

import (
	"fmt"
	"net/http"

	"days7.test/web/context/ctx"
)

func main() {
	r := ctx.New()
	r.GET("/", func(c *ctx.Context) {
		c.HTML(http.StatusOK, "Welcome to the wuhan")
	})

	r.GET("/hello", func(c *ctx.Context) {
		// expect /hello?name=pardon110
		c.String(http.StatusOK, "hello %s, you're at %s\n", c.Query("name"), c.Path)
	})

	r.POST("/login", func(c *ctx.Context) {
		c.JSON(http.StatusOK, ctx.H{
			"username": c.PostForm("username"),
			"password": c.PostForm("password"),
		})
	})

	if err := r.Run(":8080"); err != nil {
		fmt.Println(err)
	}
}
