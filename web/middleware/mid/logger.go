/*
 * @Author: pardon110
 * @Date: 2024-03-18 10:56:23
 * @LastEditors: pardon110@qq.com
 * @LastEditTime: 2024-03-18 11:14:25
 * @FilePath: \days7\web\middleware\mid\logger.go
 * @Description:
 */
package mid

import (
	"log"
	"time"
)

// 全局中间件
func Logger() HandlerFunc {
	// 前后置中间件
	return func(c *Context) {
		t := time.Now()

		c.Next()

		log.Printf("[%d] %s in %v", c.StatusCode, c.Req.RequestURI, time.Since(t).Nanoseconds())
	}
}
