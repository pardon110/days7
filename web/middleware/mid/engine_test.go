/*
 * @Author: pardon110
 * @Date: 2024-03-18 10:52:46
 * @LastEditors: pardon110@qq.com
 * @LastEditTime: 2024-03-18 10:53:10
 * @FilePath: \days7\web\middleware\mid\engine_test.go
 * @Description:
 */
package mid

import (
	"testing"
)

func TestRouterGroup_Group(t *testing.T) {
	r := New()
	v1 := r.Group("/v1")
	v2 := v1.Group("/v2")
	v3 := v2.Group("/v3")

	if v2.prefix != "/v1/v2" {
		t.Fatal("v2 prefix should be /v1/v2")
	}
	if v3.prefix != "/v1/v2/v3" {
		t.Fatal("v2 prefix should be /v1/v2")
	}
}
