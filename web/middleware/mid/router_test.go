/*
 * @Author: pardon110
 * @Date: 2024-03-17 18:48:10
 * @LastEditors: pardon110@qq.com
 * @LastEditTime: 2024-03-18 10:54:19
 * @FilePath: \days7\web\middleware\mid\router_test.go
 * @Description:
 */
package mid

import (
	"fmt"
	"reflect"
	"testing"
)

func newTestRouter() *router {
	r := newRouter()
	r.addRoute("GET", "/", nil)
	r.addRoute("GET", "/hello/:name", nil)
	r.addRoute("GET", "/hello/b/c", nil)
	r.addRoute("GET", "/hi/:name", nil)
	r.addRoute("GET", "/assets/*filepath", nil)
	return r
}

func Test_parsePattern(t *testing.T) {
	tests := []struct {
		name string
		path string
		want []string
	}{
		{"name_params", "/p/:name", []string{"p", ":name"}},
		{"match_params", "/p/*", []string{"p", "*"}},
		{"match_name_params", "/p/*name/*", []string{"p", "*name"}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := parsePattern(tt.path); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("parsePattern() = %v, want %v", got, tt.want)
			}
		})
	}
}

// go test -v -run ^Test_router_getRoutes$ days7.test/web/router/trie
// isWild
func Test_router_getRoutes(t *testing.T) {
	r := newTestRouter()
	nodes := r.getRoutes("GET")
	for i, n := range nodes {
		fmt.Println(i+1, n)
	}

	if len(nodes) != 5 {
		t.Fatal("the number of routes shoule be 5")
	}
}

// 动态路由访问
func Test_router_getRoute(t *testing.T) {
	r := newTestRouter()
	n, params := r.getRoute("GET", "/hello/pardon110")

	if n == nil {
		t.Fatal("nil shouldn't be returned")
	}

	if n.pattern != "/hello/:name" {
		t.Fatal("should match /hello/:name")
	}

	if params["name"] != "pardon110" {
		t.Fatal("name should be equal to 'pardon110'")
	}

	fmt.Printf("matched path: %s, params['name']: %s\n", n.pattern, params["name"])
}

// 静态资源访问
func Test_assets_router_getRoute(t *testing.T) {
	r := newTestRouter()
	n1, ps1 := r.getRoute("GET", "/assets/file1.txt")
	ok1 := n1.pattern == "/assets/*filepath" && ps1["filepath"] == "file1.txt"
	if !ok1 {
		t.Fatal("pattern shoule be /assets/*filepath & filepath shoule be file1.txt")
	}

	n2, ps2 := r.getRoute("GET", "/assets/css/test.css")
	ok2 := n2.pattern == "/assets/*filepath" && ps2["filepath"] == "css/test.css"
	if !ok2 {
		t.Fatal("pattern shoule be /assets/*filepath & filepath shoule be css/test.css")
	}
}
