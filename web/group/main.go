/*
 * @Author: pardon110
 * @Date: 2024-03-17 16:58:12
 * @LastEditors: pardon110@qq.com
 * @LastEditTime: 2024-03-18 08:54:34
 * @FilePath: \days7\web\group\main.go
 * @Description:
 */
package main

import (
	"net/http"

	"days7.test/web/group/grp"
)

func main() {
	r := grp.New()
	r.GET("/index", func(c *grp.Context) {
		c.HTML(http.StatusOK, "Welcome to Index Page")
	})
	v1 := r.Group("/v1")
	{
		v1.GET("/", func(c *grp.Context) {
			c.HTML(http.StatusOK, "Hi grp!")
		})
		v1.GET("/hello", func(c *grp.Context) {
			c.String(http.StatusOK, "hello %s, your're at %s\n", c.Query("name"), c.Path)
		})
	}
	v2 := r.Group("/v2")
	{
		v2.GET("/hello/:name", func(c *grp.Context) {
			// expect /hello/pardon110
			c.String(http.StatusOK, "hello %s, you're at %s\n", c.Param("name"), c.Path)
		})
		v2.POST("/login", func(c *grp.Context) {
			c.JSON(http.StatusOK, grp.H{
				"username": c.PostForm("username"),
				"password": c.PostForm("password"),
			})
		})

	}

	r.Run(":8080")
}
