/*
 * @Author: pardon110
 * @Date: 2024-03-26 10:32:12
 * @LastEditors: pardon110@qq.com
 * @LastEditTime: 2024-03-26 10:55:53
 * @FilePath: \days7\orm\sqlop\session\raw_test.go
 * @Description:
 */
package session

import (
	"database/sql"
	"os"
	"testing"

	_ "github.com/mattn/go-sqlite3"
)

var TestDB *sql.DB

// 首尾清理工作
// testing.M 管理测试的运行和状态
func TestMain(m *testing.M) {
	TestDB, _ = sql.Open("sqlite3", "../engine.db")
	code := m.Run()
	_ = TestDB.Close()
	os.Exit(code)
}

// session 类型转换
func NewSession() *Session {
	return New(TestDB)
}

func TestSession_Exec(t *testing.T) {
	s := NewSession()
	_, _ = s.Raw("DROP TABLE IF EXISTS User;").Exec()
	_, _ = s.Raw("CREATE TABLE User(Name text);").Exec()
	result, _ := s.Raw("INSERT INTO User(`Name`) values (?), (?),(?)", "Tom", "Sam", "pardon").Exec()
	if count, err := result.RowsAffected(); err != nil || count != 3 {
		t.Fatal("expect 2, but got", count)
	}
}

func TestSession_QueryRows(t *testing.T) {
	s := NewSession()
	_, _ = s.Raw("DROP TABLE IF EXISTS User;").Exec()
	_, _ = s.Raw("CREATE TABLE User(Name text);").Exec()
	row := s.Raw("SELECT count(*) FROM User WHERE Name").QueryRow()
	var count int
	if err := row.Scan(&count); err != nil || count != 0 {
		t.Fatal("failed to query db", err)
	}
}
