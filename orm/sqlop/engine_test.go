/*
 * @Author: pardon110
 * @Date: 2024-03-25 17:39:09
 * @LastEditors: pardon110@qq.com
 * @LastEditTime: 2024-03-26 10:33:31
 * @FilePath: \days7\orm\sqlop\engine_test.go
 * @Description:
 */

package sqlop

import (
	"testing"

	_ "github.com/mattn/go-sqlite3"
)

// go test -v -run ^TestNewEngine$ days7.test/orm/sqlop
func TestNewEngine(t *testing.T) {
	engine := Open(t)
	defer engine.Close()
}

func Open(t *testing.T) *Engine {
	// 打印行信息
	// t.Helper()
	engine, err := NewEngine("sqlite3", "engine.db")
	if err != nil {
		t.Fatal("failed to create engine", err)
	}
	return engine
}
