/*
* @Author: pardon110
* @Date: 2024-03-26 18:54:40
 * @LastEditors: pardon110@qq.com
 * @LastEditTime: 2024-03-27 09:57:43
 * @FilePath: \days7\orm\clause\clause_test.go

* @Description:
1）多次调用 clause.Set() 构造好每一个子句。
2）调用一次 clause.Build() 按照传入的顺序构造出最终的 SQL 语句。
构造完成后，调用 Raw().Exec() 方法执行
*/

package clause

import (
	"reflect"
	"testing"
)

// go test -v -count=1 -run ^TestClause_Set$ days7.test/orm/clause
func TestClause_Set(t *testing.T) {
	var clause Clause
	clause.Set(INSERT, "User", []string{"Name", "Age"})
	sql := clause.sql[INSERT]
	vars := clause.sqlVars[INSERT]
	t.Log(sql, vars)

	if sql != "INSERT INTO User (Name,Age)" || len(vars) != 0 {
		t.Fatal("failed to get clause")
	}
}

// go test -v -count=1 -run ^TestClause_Build$ days7.test/orm/clause
func TestClause_Build(t *testing.T) {
	t.Run("select", func(t *testing.T) {
		testSelect(t)
	})
}

// where
func testSelect(t *testing.T) {
	var clause Clause
	clause.Set(LIMIT, 3)
	clause.Set(SELECT, "User", []string{"*"})
	clause.Set(WHERE, "Name = ?", "Tom")
	clause.Set(ORDERBY, "Age ASC")
	sql, vars := clause.Build(SELECT, WHERE, ORDERBY, LIMIT)
	t.Log(sql, vars)
	if sql != "SELECT * FROM User WHERE Name = ? ORDER BY Age ASC LIMIT ?" {
		t.Fatal("failed to build SQL")
	}
	t.Log("last", []interface{}{"Tom", 3})
	if !reflect.DeepEqual(vars, []interface{}{"Tom", 3}) {
		t.Fatal("failed to build SQLVars")
	}

}
