/*
 * @Author: pardon110
 * @Date: 2024-03-26 12:20:40
 * @LastEditors: pardon110@qq.com
 * @LastEditTime: 2024-03-26 12:23:34
 * @FilePath: \days7\orm\dialect\sqlite3_test.go
 * @Description:
 */
package dialect

import (
	"reflect"
	"testing"
)

func Test_sqlite3_DataTypeOf(t *testing.T) {
	dial := &sqlite3{}
	tests := []struct {
		Value interface{}
		Type  string
	}{
		{"Tom", "text"},
		{123, "integer"},
		{1.2, "real"},
		{[]int{1, 2, 3}, "blob"},
	}
	for _, c := range tests {
		if typ := dial.DataTypeOf(reflect.ValueOf(c.Value)); typ != c.Type {
			t.Fatalf("expect %s, but got %s", c.Type, typ)
		}
	}
}
