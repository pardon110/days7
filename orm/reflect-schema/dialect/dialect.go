/*
 * @Author: pardon110
 * @Date: 2024-03-26 11:24:28
 * @LastEditors: pardon110@qq.com
 * @LastEditTime: 2024-03-26 11:30:17
 * @FilePath: \days7\orm\dialect\dialect.go
 * @Description:
 */
package dialect

import "reflect"

var dialectsMap = map[string]Dialect{}

type Dialect interface {
	DataTypeOf(typ reflect.Value) string
	TableExistSQL(tableName string) (string, []interface{})
}

func RegisterDialect(name string, dialect Dialect) {
	dialectsMap[name] = dialect
}

func GetDialect(name string) (dialect Dialect, ok bool) {
	dialect, ok = dialectsMap[name]
	return
}
