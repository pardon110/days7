/*
 * @Author: pardon110
 * @Date: 2024-03-26 16:59:26
 * @LastEditors: pardon110@qq.com
 * @LastEditTime: 2024-03-26 18:13:10
 * @FilePath: \days7\orm\reflect-schema\session\table.go
 * @Description:
 */
package session

import (
	"fmt"
	"reflect"
	"strings"

	"days7.test/orm/log"
	"days7.test/orm/reflect-schema/schema"
)

// Model assigns refTable
// Model() 被调用多次，如果传入的结构体名称不发生变化，则不会更新 refTable 的值
func (s *Session) Model(value interface{}) *Session {
	if s.refTable == nil || reflect.TypeOf(value) != reflect.TypeOf(s.refTable.Model) {
		s.refTable = schema.Parse(value, s.dialect)
	}
	return s
}

func (s *Session) RefTable() *schema.Schema {
	if s.refTable == nil {
		log.Error("model is not initialized")
	}
	return s.refTable
}

func (s *Session) CreateTable() error {
	table := s.refTable
	var columns []string
	for _, field := range table.Fields {
		columns = append(columns, fmt.Sprintf("%s %s %s", field.Name, field.Type, field.Tag))
	}
	desc := strings.Join(columns, ",")
	_, err := s.Raw(fmt.Sprintf("CREATE TABLE %s (%s);", table.Name, desc)).Exec()
	return err
}

func (s *Session) DropTable() error {
	_, err := s.Raw(fmt.Sprintf("DROP TABLE IF EXISTS %s", s.RefTable().Name)).Exec()
	return err
}

func (s *Session) HasTable() bool {
	sql, values := s.dialect.TableExistSQL(s.RefTable().Name)
	row := s.Raw(sql, values...).QueryRow()
	var tmp string
	_ = row.Scan(&tmp)
	return tmp == s.RefTable().Name
}
