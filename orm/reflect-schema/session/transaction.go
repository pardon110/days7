/*
 * @Author: pardon110
 * @Date: 2024-03-27 15:28:37
 * @LastEditors: pardon110@qq.com
 * @LastEditTime: 2024-03-27 15:39:46
 * @FilePath: \days7\orm\reflect-schema\session\transaction.go
 * @Description:
 */
package session

import "days7.test/orm/log"

func (s *Session) Begin() (err error) {
	log.Info("transaction begin")
	if s.tx, err = s.db.Begin(); err != nil {
		log.Error(err)
	}
	return
}

func (s *Session) Commit() (err error) {
	log.Info("transaction commit")
	if err = s.tx.Commit(); err != nil {
		log.Error(err)
	}
	return
}

func (s *Session) Rollback() (err error) {
	log.Info("transaction rollback")
	if err = s.tx.Rollback(); err != nil {
		log.Error(err)
	}
	return
}
