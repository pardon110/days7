/*
 * @Author: pardon110
 * @Date: 2024-03-27 14:22:29
 * @LastEditors: pardon110@qq.com
 * @LastEditTime: 2024-03-27 15:13:51
 * @FilePath: \days7\orm\reflect-schema\session\hooks_test.go
 * @Description:  通过反射获取结构体绑定的钩子，并调用
 */

package session

import (
	"testing"

	"days7.test/orm/log"
)

type Account struct {
	ID       int `geeorm:"PRIMARY KEY"`
	Password string
}

func (account *Account) BeforeInsert(s *Session) error {
	log.Info("before insert", account)
	account.ID += 1000
	return nil
}

func (account *Account) AfterQuery(s *Session) error {
	log.Info("after query", account)
	account.Password = "******"
	return nil
}

// go test -v -count=1  -run ^TestSession_CallMethod$ days7.test/orm/reflect-schema/session

func TestSession_CallMethod(t *testing.T) {
	s := NewSession().Model(&Account{})
	_ = s.DropTable()
	_ = s.CreateTable()
	_, _ = s.Insert(&Account{1, "123456"}, &Account{2, "qwerty"})

	u := &Account{}
	err := s.First(u)
	t.Log("u value", u)
	if err != nil || u.ID != 1001 || u.Password != "******" {
		t.Fatal("Failed to call hooks after query, got", u)
	}
}
