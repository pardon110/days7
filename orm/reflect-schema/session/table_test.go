/*
 * @Author: pardon110
 * @Date: 2024-03-26 16:59:26
 * @LastEditors: pardon110@qq.com
 * @LastEditTime: 2024-03-26 18:05:21
 * @FilePath: \days7\orm\reflect-schema\session\table_test.go
 * @Description:
 */

package session

import (
	"fmt"
	"testing"
)

type User struct {
	Name string `geeorm:"PRIMARY KEY"`
	Age  int
}

type ABC struct {
	Name string `geeorm:"PRIMARY KEY"`
	Age  int
}

func TestSession_CreateTable(t *testing.T) {
	s := NewSession().Model(&ABC{})
	_ = s.DropTable()
	_ = s.CreateTable()
	if !s.HasTable() {
		t.Fatal("Failed to create table User")
	}
}

// go test -v -run ^TestSession_Model$ days7.test/orm/reflect-schema/session
func TestSession_Model(t *testing.T) {
	s := NewSession().Model(&User{})
	table := s.RefTable()
	fmt.Println("1---------", table.Name)
	s.Model(&Session{})
	fmt.Println("2---------", s.refTable.Name)
	if table.Name != "User" || s.RefTable().Name != "Session" {
		t.Fatal("Failed to change model")
	}
}
