/*
 * @Author: pardon110
 * @Date: 2024-03-25 20:04:02
 * @LastEditors: pardon110@qq.com
 * @LastEditTime: 2024-03-25 20:19:25
 * @FilePath: \days7\orm\log\log_test.go
 * @Description:
 */

package log

import (
	"os"
	"testing"
)

func TestSetLevel(t *testing.T) {
	SetLevel(ErrorLevel)
	if infoLog.Writer() == os.Stdout || errorLog.Writer() != os.Stdout {
		t.Fatal("failed to set log level")
	}
	SetLevel(Disabled)
	if infoLog.Writer() == os.Stdout || errorLog.Writer() == os.Stdout {
		t.Fatal("failed to set log level")
	}
}
