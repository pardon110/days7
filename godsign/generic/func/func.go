/*
 * @Author: pardon110
 * @Date: 2024-03-29 08:55:22
 * @LastEditors: pardon110@qq.com
 * @LastEditTime: 2024-03-29 09:24:09
 * @FilePath: \days7\godsign\generic\generic.go
 * @Description:
 */
package main

import "cmp"

func maxValue[T cmp.Ordered](x, y T) T {
	if x > y {
		return x
	}
	return y
}

func main() {
	// 实例化， 类型实参
	println(maxValue[int](1, 2))
	// 类型推导，类型实参省略
	println(maxValue(1.1, 1.2))

}
