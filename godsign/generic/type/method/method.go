package main

type A int
type B string

func (a A) Test() { println("A: ", a) }
func (b B) Test() { println("B: ", b) }

type Tester interface {
	A | B
	Test()
}

func test[T Tester](x T) {
	x.Test() //约束调用
}

func main() {
	test[A](1)
	test[B]("abc")
}
