/*
 * @Author: pardon110
 * @Date: 2024-03-29 09:25:31
 * @LastEditors: pardon110@qq.com
 * @LastEditTime: 2024-03-29 09:27:34
 * @FilePath: \days7\godsign\generic\type\struct.go
 * @Description:
 */
package main

import "fmt"

type Data[T any] struct {
	x T
}

func (d Data[T]) test() {
	fmt.Println(d)
}

func main() {
	d := Data[int]{x: 1}
	d.test()
}
