/*
 * @Author: pardon110
 * @Date: 2024-03-29 09:29:45
 * @LastEditors: pardon110@qq.com
 * @LastEditTime: 2024-03-29 09:41:29
 * @FilePath: \days7\godsign\generic\type\stringer\main.go
 * @Description:
 */
package main

import "fmt"

type N struct {
}

func (N) String() string {
	return "N"
}

func test[T fmt.Stringer](v T) {
	fmt.Println(v)
}

func main() {
	// test(1)
	test(N{})
}

type Integer interface {
	Signed | Unsigned
}

type Signed interface {
	~int | ~int8 | ~int16 | ~int32 | ~int64
}

type Unsigned interface {
	~uint | ~uint8 | ~uint16 | ~uint32 | ~uint64
}
