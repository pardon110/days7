/*
 * @Author: pardon110
 * @Date: 2024-03-29 11:16:04
 * @LastEditors: pardon110@qq.com
 * @LastEditTime: 2024-03-29 11:30:07
 * @FilePath: \days7\godsign\concurrency\race\test.go
 * @Description:
 */
package main

import (
	"fmt"
	"sync"
)

func main() {
	var wg sync.WaitGroup
	wg.Add(2)

	x := 0

	go func() {
		defer wg.Done()
		x++
	}()

	go func() {
		defer wg.Done()
		x--
	}()

	wg.Wait()

	fmt.Println("Final value of x:", x)
}
