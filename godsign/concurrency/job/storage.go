package main

import (
	"fmt"
	"sync"
)

func main() {
	var gls [2]struct {
		id  int
		ret int
	}

	var wg sync.WaitGroup
	wg.Add(len(gls))

	for i := 0; i < len(gls); i++ {
		go func(id int) {
			defer wg.Done()

			gls[id].id = id
			gls[id].ret = (i + 1) * 100
		}(i)
	}

	wg.Wait()
	fmt.Printf("%+v\n", gls)
}
