/*
 * @Author: pardon110
 * @Date: 2024-03-29 10:45:41
 * @LastEditors: pardon110@qq.com
 * @LastEditTime: 2024-03-29 10:48:07
 * @FilePath: \days7\godsign\concurrency\chan\async\async.go
 * @Description:
 */
package main

func main() {
	quit := make(chan struct{})
	data := make(chan int, 3)

	data <- 11
	data <- 22
	data <- 33

	println(cap(data), len(data))

	go func() {
		defer close(quit) // close the channel notify main goroutine

		println(<-data)
		println(<-data)
		println(<-data)

		println(<-data) // block
	}()

	data <- 44
	<-quit
}
