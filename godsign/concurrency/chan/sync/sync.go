package main

func main() {
	quit := make(chan struct{})
	data := make(chan int)

	go func() {
		data <- 11
	}()

	go func() {
		defer close(quit)

		println(<-data)
		println(<-data)
	}()
	data <- 22
	<-quit
}
