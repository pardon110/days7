package main

import "bytes"

type X = bytes.Buffer

// cannot define new methods on non-local type bytes.Buffer
// func (x *X) B() {
// 	println("X.b")
// }

func main() {

}
