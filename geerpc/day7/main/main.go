/*
 * @Author: pardon110
 * @Date: 2024-03-25 16:34:56
 * @LastEditors: pardon110@qq.com
 * @LastEditTime: 2024-03-25 17:10:20
 * @FilePath: \days7\geerpc\day7\main\main.go
 * @Description:
 */
package main

import (
	"context"
	"log"
	"net"
	"net/http"
	"sync"
	"time"

	"days7.test/geerpc/day7"
	"days7.test/geerpc/day7/registry"
	"days7.test/geerpc/day7/xclient"
)

type Foo int

type Args struct{ Num1, Num2 int }

func (f Foo) Sum(args Args, reply *int) error {
	*reply = args.Num1 + args.Num2
	return nil
}

func (f Foo) Sleep(args Args, reply *int) error {
	time.Sleep(time.Second * time.Duration(args.Num1))
	*reply = args.Num1 + args.Num2
	return nil
}

func startRegistry(wg *sync.WaitGroup) {
	l, _ := net.Listen("tcp", ":9527")
	// 注册路由
	registry.HandleHTTP()
	// 必须处于此位置，下一句监听阻塞，defer 对于内有阻塞部分无效
	wg.Done()
	// 启动监听
	err := http.Serve(l, nil)
	log.Println("error", err)
}

func startServer(registerAddr string, wg *sync.WaitGroup) {
	var foo Foo
	l, _ := net.Listen("tcp", ":0")
	server := day7.NewServer()
	_ = server.Register(&foo)

	//  启动服务时，向注册中心发送心跳保活
	registry.Heartbeat(registerAddr, "tcp@"+l.Addr().String(), 0)
	wg.Done()
	server.Accept(l)
}

func foo(xc *xclient.XClient, ctx context.Context, typ, serviceMethod string, args *Args) {
	var reply int
	var err error
	switch typ {
	case "call":
		err = xc.Call(ctx, serviceMethod, args, &reply)
	case "broadcast":
		err = xc.Broadcast(ctx, serviceMethod, args, &reply)
	}
	if err != nil {
		log.Printf("%s %s error: %v", typ, serviceMethod, err)
	} else {
		log.Printf("%s %s success: %d + %d = %d", typ, serviceMethod, args.Num1, args.Num2, reply)
	}
}

func call(registry string) {
	d := xclient.NewGeeRegistryDiscovery(registry, 0)
	xc := xclient.NewXClient(d, xclient.RandomSelect, nil)
	defer func() { _ = xc.Close() }()
	// send request & receive response
	var wg sync.WaitGroup
	for i := 0; i < 5; i++ {
		wg.Add(1)
		go func(i int) {
			defer wg.Done()
			foo(xc, context.Background(), "call", "Foo.Sum", &Args{Num1: i, Num2: i * i})
		}(i)
	}
	wg.Wait()
}

func broadcast(registry string) {
	d := xclient.NewGeeRegistryDiscovery(registry, 0)
	xc := xclient.NewXClient(d, xclient.RandomSelect, nil)
	defer func() { _ = xc.Close() }()
	var wg sync.WaitGroup
	for i := 0; i < 5; i++ {
		wg.Add(1)
		go func(i int) {
			defer wg.Done()
			foo(xc, context.Background(), "broadcast", "Foo.Sum", &Args{Num1: i, Num2: i * i})
			// expect 2 - 5 timeout
			ctx, cancel := context.WithTimeout(context.Background(), time.Second*2)
			defer cancel()
			foo(xc, ctx, "broadcast", "Foo.Sleep", &Args{Num1: i, Num2: i * i})
		}(i)
	}
	wg.Wait()
}

func main() {

	log.SetFlags(0)
	registryAddr := "http://127.0.0.1:9527/_geerpc_/registry"

	var wg sync.WaitGroup

	// 1. 启动注册中心
	wg.Add(1)
	go startRegistry(&wg)
	wg.Wait()

	time.Sleep(time.Second)

	// 2. 启动服务并注册
	wg.Add(2)
	go startServer(registryAddr, &wg)
	go startServer(registryAddr, &wg)
	wg.Wait()

	// 3. 服务调用
	time.Sleep(time.Second)
	call(registryAddr)
	broadcast(registryAddr)
}
