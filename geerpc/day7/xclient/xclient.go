/*
 * @Author: pardon110
 * @Date: 2024-03-24 17:58:11
 * @LastEditors: pardon110@qq.com
 * @LastEditTime: 2024-03-25 11:46:09
 * @FilePath: \days7\geerpc\day6\xclient\xclient.go
 * @Description: 负载均衡
 */
package xclient

import (
	"context"
	"io"
	"reflect"
	"sync"

	. "days7.test/geerpc/day6"
)

type XClient struct {
	d       Discovery
	mode    SelectMode
	opt     *Option
	mu      sync.Mutex
	clients map[string]*Client
}

var _ io.Closer = (*XClient)(nil)

func NewXClient(d Discovery, mode SelectMode, opt *Option) *XClient {
	return &XClient{
		d:       d,
		mode:    mode,
		opt:     opt,
		clients: make(map[string]*Client),
	}
}

// Close implements io.Closer.
func (x *XClient) Close() error {
	x.mu.Lock()
	defer x.mu.Unlock()
	for key, client := range x.clients {
		// no idea how to deal with error, just ignore it.
		_ = client.Close()
		delete(x.clients, key)
	}
	return nil
}

func (x *XClient) dial(rpcAddr string) (*Client, error) {
	x.mu.Lock()
	defer x.mu.Unlock()

	client, ok := x.clients[rpcAddr]
	if ok && !client.IsAvaliable() {
		_ = client.Close()
		delete(x.clients, rpcAddr)
		client = nil
	}
	if client == nil {
		var err error
		client, err = XDial(rpcAddr, x.opt)
		if err != nil {
			return nil, err
		}
		x.clients[rpcAddr] = client
	}
	return client, nil
}

func (x *XClient) call(rpcAddr string, ctx context.Context, serviceMethod string, args, reply interface{}) error {
	client, err := x.dial(rpcAddr)
	if err != nil {
		return err
	}
	return client.Call(ctx, serviceMethod, args, reply)
}

// load balancer
func (x *XClient) Call(ctx context.Context, serviceMethod string, args, reply interface{}) error {
	rpcAddr, err := x.d.Get(x.mode)
	if err != nil {
		return err
	}
	return x.call(rpcAddr, ctx, serviceMethod, args, reply)
}

func (x *XClient) Broadcast(ctx context.Context, serviceMethod string, args, reply interface{}) error {
	servers, err := x.d.GetAll()
	if err != nil {
		return err
	}
	var wg sync.WaitGroup
	var mu sync.Mutex
	var e error
	replyDone := reply == nil
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()
	for _, rpcAddr := range servers {
		wg.Add(1)
		go func(rpcAddr string) {
			defer wg.Done()
			var clonedReply interface{}
			if reply != nil {
				clonedReply = reflect.New(reflect.ValueOf(reply).Elem().Type()).Interface()
			}
			err := x.call(rpcAddr, ctx, serviceMethod, args, clonedReply)

			// Broadcast 将请求广播到所有的服务实例，如果任意一个实例发生错误，则返回其中一个错误；如果调用成功，则返回其中一个的结果。
			// 为了提升性能，请求是并发的。
			// **并发情况下需要使用互斥锁保证 error 和 reply 能被正确赋值。**
			// 借助 context.WithCancel 确保有错误发生时，快速失败。
			// 锁临界区缩小是为了方便x.call调用并发进行
			// 锁保护对 replyDone 标识的并发读写操作
			mu.Lock()
			if err != nil && e == nil {
				e = err
				cancel()
			}
			if err == nil && !replyDone {
				reflect.ValueOf(reply).Elem().Set(reflect.ValueOf(clonedReply).Elem())
				replyDone = true
			}
			mu.Unlock()
		}(rpcAddr)
	}
	wg.Wait()
	return e
}
