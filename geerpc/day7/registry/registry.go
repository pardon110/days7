/*
 * @Author: pardon110
 * @Date: 2024-03-25 14:53:23
 * @LastEditors: pardon110@qq.com
 * @LastEditTime: 2024-03-25 17:15:40
 * @FilePath: \days7\geerpc\day7\registry\registry.go
 * @Description:
 */
package registry

import (
	"log"
	"net/http"
	"sort"
	"strings"
	"sync"
	"time"
)

type ServerItem struct {
	Addr  string
	start time.Time
}

//	add a server and receive heartbeat to keep it alive.
//	returns all alive servers and delete dead servers sync simultaneously.
//
// GeeRegistry
type GeeRegistry struct {
	timeout time.Duration
	mu      sync.Mutex
	servers map[string]*ServerItem
}

// aliveServers implements Registry.
func (g *GeeRegistry) aliveServers() []string {
	g.mu.Lock()
	defer g.mu.Unlock()
	var alive []string
	for addr, s := range g.servers {
		if g.timeout == 0 || s.start.Add(g.timeout).After(time.Now()) {
			alive = append(alive, addr)
		} else {
			delete(g.servers, addr)
		}
	}
	sort.Strings(alive)
	return alive
}

// putServer implements Registry.
func (g *GeeRegistry) putServer(addr string) {
	g.mu.Lock()
	defer g.mu.Unlock()

	s := g.servers[addr]
	if s == nil {
		g.servers[addr] = &ServerItem{Addr: addr, start: time.Now()}
	} else {
		s.start = time.Now() // if exists, update start time to keep alive
	}
}

// 具备接收外部通信的能力
// 1. 提供服务列表查询  2. 支持服务更新
// Runs at /_geerpc_/registry
// // keep it simple, server is in req.Header
func (g *GeeRegistry) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "GET":
		w.Header().Set("X-Geerpc-Servers", strings.Join(g.aliveServers(), ","))
	case "POST":
		addr := r.Header.Get("X-Geerpc-Server")
		if addr == "" {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		// 更新注册信息
		g.putServer(addr)
	default:
		w.WriteHeader(http.StatusMethodNotAllowed)
	}
}

const (
	defaultPath    = "/_geerpc_/registry"
	defaultTimeout = time.Minute * 5
)

func New(timeout time.Duration) *GeeRegistry {
	return &GeeRegistry{
		servers: make(map[string]*ServerItem),
		timeout: timeout,
	}
}

var DefaultGeeRegister = New(defaultTimeout)

func (g *GeeRegistry) HandleHTTP(registryPath string) {
	http.Handle(registryPath, g)
	log.Println("rpc registry path:", registryPath)
}

func HandleHTTP() {
	DefaultGeeRegister.HandleHTTP(defaultPath)
}

// 服务实例发送心跳，注册中心被动接收执行调度管理 单向
func sendHeartbeat(registry, addr string) error {
	log.Println(addr, "send heart beat to registry", registry)
	httpClient := &http.Client{}
	req, _ := http.NewRequest("POST", registry, nil)
	req.Header.Set("X-Geerpc-Server", addr)
	if _, err := httpClient.Do(req); err != nil {
		log.Println("rpc server: heart beat err:", err)
		return err
	}
	return nil
}

// Heartbeat 便于服务启动时定时向注册中心发送心跳,更新注册信息
func Heartbeat(registry, addr string, duration time.Duration) {
	if duration == 0 {
		// make sure there is enough time to send heart beat
		// before it's removed from registry
		duration = defaultTimeout - time.Duration(1)*time.Minute
	}
	var err error
	err = sendHeartbeat(registry, addr)
	go func() {
		t := time.NewTicker(duration)
		for err == nil {
			<-t.C
			err = sendHeartbeat(registry, addr)
		}
	}()
}
