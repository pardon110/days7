/*
 * @Author: pardon110
 * @Date: 2024-03-23 15:15:02
 * @LastEditors: pardon110@qq.com
 * @LastEditTime: 2024-03-23 17:39:40
 * @FilePath: \days7\geerpc\day3\main\main_test.go
 * @Description:
 */

package main

import (
	"log"
	"reflect"
	"strings"
	"sync"
	"testing"
)

// go test -v days7.test/geerpc/day3/main
func Test_reflect(t *testing.T) {
	var wg sync.WaitGroup

	typ := reflect.TypeOf(&wg)
	for i := 0; i < typ.NumMethod(); i++ {
		method := typ.Method(i)

		argv := make([]string, 0, method.Type.NumIn())     // 入参 接收者算一个，多1个
		returns := make([]string, 0, method.Type.NumOut()) // 出参

		for j := 0; j < method.Type.NumIn(); j++ {
			argv = append(argv, method.Type.In(j).Name())
		}

		for j := 0; j < method.Type.NumOut(); j++ {
			returns = append(returns, method.Type.Out(j).Name())
		}

		log.Printf("func (w *%s) %s(%s) %s  params number: %d",
			typ.Elem().Name(),
			method.Name,
			strings.Join(argv, ", "),
			strings.Join(returns, ", "),
			method.Type.NumIn(),
		)
	}
}
