/*
 * @Author: pardon110
 * @Date: 2024-03-23 15:14:50
 * @LastEditors: pardon110@qq.com
 * @LastEditTime: 2024-03-23 18:02:54
 * @FilePath: \days7\geerpc\day3\service_test.go
 * @Description:
 *
 *	func (t *T) MethodName(argType T1, replyType *T2) error
 */

package day3

import (
	"fmt"
	"reflect"
	"testing"
)

type Foo int

type Args struct{ Num1, Num2 int }

func (f Foo) Sum(args Args, reply *int) error {
	*reply = args.Num1 + args.Num2
	return nil
}

// it's not a exported Method
func (f Foo) sum(args Args, reply *int) error {
	*reply = args.Num1 + args.Num2
	return nil
}

func _assert(condition bool, format string, v ...interface{}) {
	if !condition {
		panic(fmt.Sprintf("assertion failed: "+format, v...))
	}

}

func Test_newService(t *testing.T) {
	var foo Foo
	s := newService(&foo)
	_assert(len(s.method) == 1, "wrong service Method, expect 1, but got %d", len(s.method))

	mType := s.method["Sum"]
	_assert(mType != nil, "wrong Method, Sum shouldn't nil")
}

func Test_service_call(t *testing.T) {
	var foo Foo
	s := newService(&foo)
	mType := s.method["Sum"]

	argv := mType.newArgv()
	replyv := mType.newReplyv()

	argv.Set(reflect.ValueOf(Args{Num1: 1, Num2: 3}))
	err := s.call(mType, argv, replyv)
	// 先通过 replyv.Interface().(*int) 得到一个指向 int 的指针，
	// 然后通过 * 操作符解引用该指针，得到其指向的值，最后将该值与 4 进行比较
	_assert(err == nil && *replyv.Interface().(*int) == 4 && mType.NumCalls() == 1, "failed to call Foo.Sum")
}
