/*
 * @Author: pardon110
 * @Date: 2024-03-23 11:40:24
 * @LastEditors: pardon110@qq.com
 * @LastEditTime: 2024-03-23 11:57:22
 * @FilePath: \days7\geerpc\day2\main\main.go
 * @Description:
 */
package main

import (
	"fmt"
	"log"
	"net"
	"sync"
	"time"

	"days7.test/geerpc/day2"
)

func startServer(addr chan<- string) {
	l, err := net.Listen("tcp", ":0")
	if err != nil {
		log.Fatal("network error: ", err)
	}

	log.Println("day2 start rpc server on", l.Addr())

	addr <- l.Addr().String()

	day2.Accept(l)
}

func main() {
	log.SetFlags(0)
	addr := make(chan string)
	go startServer(addr)

	client, _ := day2.Dial("tcp", <-addr)
	defer func() {
		_ = client.Close()
	}()

	time.Sleep(time.Second)

	var wg sync.WaitGroup
	for i := 0; i < 50; i++ {
		wg.Add(1)
		func(i int) {
			defer wg.Done()
			args := fmt.Sprintf("day2 rpc req %d", i)
			var reply string
			if err := client.Call("Day2Service.Sum", args, &reply); err != nil {
				log.Fatal("call Foo.Sum error:", err)
			}
			log.Println("reply:", reply)
		}(i)

	}
	wg.Wait()
}
