/*
 * @Author: pardon110
 * @Date: 2024-03-24 09:53:53
 * @LastEditors: pardon110@qq.com
 * @LastEditTime: 2024-03-24 10:42:46
 * @FilePath: \days7\geerpc\day4\client_test.go
 * @Description: go test -v -timeout 5s days7.test/geerpc/day4
 */
package day4

import (
	"context"
	"net"
	"strings"
	"testing"
	"time"
)

// 连接超时
func TestClient_dialTimeout(t *testing.T) {
	// When a test is run multiple times due to use of -test.count or -test.cpu,
	// 并发测试标识
	t.Parallel()

	l, _ := net.Listen("tcp", ":0")

	f := func(conn net.Conn, opt *Option) (client *Client, err error) {
		_ = conn.Close()
		// 模拟服务端连接需时2s
		time.Sleep(time.Second * 2)
		return nil, nil
	}

	t.Run("timeout", func(t *testing.T) {
		// 客户端发起连接期望 1s内
		_, err := dialTimeout(f, "tcp", l.Addr().String(), &Option{ConnectTimeout: time.Second})
		_assert(err != nil && strings.Contains(err.Error(), "connect timeout"), "expect a timeout error")
	})

	t.Run("0", func(t *testing.T) {
		// 不受时间限制
		_, err := dialTimeout(f, "tcp", l.Addr().String(), &Option{ConnectTimeout: 0})
		_assert(err == nil, "0 means no limit")
	})
}

type Bar int

func (b Bar) Timeout(argv int, reply *int) error {
	time.Sleep(time.Second * 2)
	return nil
}

func startSever(addr chan<- string) {
	var b Bar
	_ = Register(&b)
	l, _ := net.Listen("tcp", ":0")
	addr <- l.Addr().String()

	Accept(l)
}

// 处理超时
func TestClient_Call(t *testing.T) {
	t.Parallel()
	addrCh := make(chan string)
	go startSever(addrCh)

	addr := <-addrCh

	time.Sleep(time.Second)

	t.Run("client timeout", func(t *testing.T) {
		client, _ := Dial("tcp", addr)
		// 客户端设置超时时间为 1s，服务端无限制
		ctx, cancel := context.WithTimeout(context.Background(), time.Second)
		defer cancel()
		var reply int
		// Bar.Timeout 耗时 2s
		err := client.Call(ctx, "Bar.Timeout", 1, &reply)
		_assert(err != nil && strings.Contains(err.Error(), ctx.Err().Error()), "expect a timeout error")
	})

	t.Run("server handle timeout", func(t *testing.T) {
		// 服务端设置超时时间为1s，客户端无限制
		client, _ := Dial("tcp", addr, &Option{
			HandleTimeout: time.Second,
		})

		var reply int
		err := client.Call(context.Background(), "Bar.Timeout", 1, &reply)
		_assert(err != nil && strings.Contains(err.Error(), "handle timeout"), "expect a timeout error")
	})
}
