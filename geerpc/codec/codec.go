/*
 * @Author: pardon110
 * @Date: 2024-03-22 10:31:53
 * @LastEditors: pardon110@qq.com
 * @LastEditTime: 2024-03-22 15:35:19
 * @FilePath: \days7\geerpc\codec\codec.go
 * @Description:
 */
package codec

import "io"

type Header struct {
	ServiceMethod string // format "Service.Method"
	Seq           uint64 // sequence number chosen by client
	Error         string
}

type Codec interface {
	io.Closer
	ReadHeader(*Header) error
	ReadBody(interface{}) error
	Write(*Header, interface{}) error
}

type NewCodecFunc func(io.ReadWriteCloser) Codec

type Type string

const (
	GobType  Type = "application/gob"
	JsonType Type = "application/json" // not implemented
)

var NewCodecFuncMap map[Type]NewCodecFunc

func init() {
	NewCodecFuncMap = make(map[Type]NewCodecFunc)
	// 持有生产Codec实例的构造函数
	NewCodecFuncMap[GobType] = NewGobCodec
}
