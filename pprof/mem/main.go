/*
 * @Author: pardon110
 * @Date: 2024-03-28 15:07:39
 * @LastEditors: pardon110@qq.com
 * @LastEditTime: 2024-03-28 15:19:53
 * @FilePath: \days7\pprof\mem\main.go
 * @Description:
 */
package main

import "math/rand"
import "github.com/pkg/profile"

const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

func randomString(n int) string {
	b := make([]byte, n)
	for i := range b {
		b[i] = letterBytes[rand.Intn(len(letterBytes))]
	}
	return string(b)
}

func concat(n int) string {
	s := ""
	for i := 0; i < n; i++ {
		s += randomString(n)
	}
	return s
}

func main() {
	defer profile.Start(profile.MemProfile, profile.MemProfileRate(1)).Stop()

	concat(100)
}
