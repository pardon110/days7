/*
 * @Author: pardon110
 * @Date: 2024-03-29 16:11:02
 * @LastEditors: pardon110@qq.com
 * @LastEditTime: 2024-03-29 16:12:52
 * @FilePath: \days7\pprof\online\main.go
 * @Description: 在线采样
 * 向目标程序注入 net/http/pprof 包
 */
package main

import (
	"net/http"
	_ "net/http/pprof"
)

func main() {
	http.ListenAndServe(":6060", http.DefaultServeMux)
}
